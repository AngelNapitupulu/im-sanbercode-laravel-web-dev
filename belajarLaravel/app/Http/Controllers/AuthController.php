<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register.form');
    }

    public function table1()
    {
        return view('register.table1');
    }
}
