@extends('layout.table')

@section('Judul')
Data Pemain Film
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key =>  $item)
    <tr>
      <th scope="row">{{ $key + 1 }}</th>
      <td>{{ $item->name }}</td>
      <td>
        <form action="/cast/{{ $item->id }}" method="post">
            @method('delete')
            @csrf
            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
        </form>
      </td>
    </tr>
    @empty
    <h1>Data Pemain Film Kosong</h1>
    @endforelse

  </tbody>
</table>
@endsection