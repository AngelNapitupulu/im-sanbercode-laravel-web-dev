@extends('layout.table')

@section('Judul')
Pemain Film
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Umur</label>
    <input type="age" name="umur" class="form-control @error('umur') is-invalid @enderror">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Biodata</label>
    <input type="text" name="biodata" class="form-control @error('biodata') is-invalid @enderror" >
  </div>
  @error('biodata')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection