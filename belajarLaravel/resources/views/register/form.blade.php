@extends('layout.table')

@section('Judul')
Sign Up Form
@endsection

@section('content')
<form action="/signup" method="GET">
    @csrf
        <label>First Name:</label><br>
        <input type="text" name="fname"><br><br>

        <label>Last Name:</label><br>
        <input type="text" name="iname"><br><br>

        <label>Gender:</label><br>
        <input type="radio" name="Gender">Male<br>
        <input type="radio" name="Gender">Female<br>
        <input type="radio" name="Gender">Other<br><br>

        <label>Nationality:</label><br>
        <select>
            <option>Indonesia</option><br>
            <option>Amerika</option><br>
            <option>Jepang</option><br>
            <option>Korea</option><br>
        </select><br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="Language Spoken">Indonesian<br>
        <input type="checkbox" name="Language Spoken">English<br>
        <input type="checkbox" name="Language Spoken">Other<br><br>

        <label>Bio:</label><br>
        <textarea name="message" rows="10" cols="30"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
@endsection


