<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new animal("shaun");

    echo "Nama :  $sheep->name <br>"; // "shaun"
    echo "Legs : $sheep->legs <br>"; // 4
    echo "Cold blooded : $sheep->cold_blooded <br>"; // "no"
    echo "<br>";
    
    $kodok = new frog("buduk");
    echo "Nama :  $kodok->name <br>"; // "Buduk"
    echo "Legs : $kodok->legs <br>"; // 4
    echo "Cold blooded : $kodok->cold_blooded <br>"; // "no"
    echo "Hop Hop" . $kodok->jump() ; // "hop hop"
    
    echo "<br>";
    echo "<br>";

    $sungokong = new ape("kera sakti");
    echo "Nama :  $sungokong->name <br>"; // "kera sakti"
    echo "Legs : $sungokong->legs <br>"; // 2
    echo "Cold blooded : $sungokong->cold_blooded <br>"; // "no"
    echo "Auooo " . $sungokong->yell(); // "Auooo"
    


?>